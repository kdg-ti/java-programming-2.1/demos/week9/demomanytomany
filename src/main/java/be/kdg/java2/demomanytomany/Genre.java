package be.kdg.java2.demomanytomany;

public enum Genre {
    THRILLER, BIOGRAPHY, FANTASY
}
