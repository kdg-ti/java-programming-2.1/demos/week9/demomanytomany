package be.kdg.java2.demomanytomany;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Component
public class DemoRunner implements CommandLineRunner {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void run(String... args) throws Exception {
        Book book1 = Book.randomBook();
        Book book2 = Book.randomBook();
        Book book3 = Book.randomBook();
        Author author1 = new Author("john","beck");
        Author author2 = new Author("mario","vargas");
        Author author3 = new Author("tine","verstrepen");
        book1.addAuthor(author1);
        author1.addBook(book1);
        book1.addAuthor(author2);
        author2.addBook(book1);
        book2.addAuthor(author1);
        author1.addBook(book2);
        book3.addAuthor(author3);
        author3.addBook(book3);
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(book1);
        em.persist(book2);
        em.persist(book3);
        em.getTransaction().commit();
        em.close();
    }
}
